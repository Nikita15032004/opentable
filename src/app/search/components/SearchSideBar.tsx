
import { Location, Cuisine, PRICE } from '@prisma/client';

import { useEffect, useState } from 'react';
import Link from 'next/link';
import { searchParamsType } from '../page';

//////////////////////////////////
export default function SearchSideBar({ locations, cuisines, queryParams }: { locations: Location[], cuisines: Cuisine[], queryParams: searchParamsType }) {

    const queries: searchParamsType = { city: queryParams.city, cuisine: queryParams.cuisine, price: queryParams.price }
    const prices = PRICE;
    return (
        <>
            <div className="w-1/5">
                <div className="border-b pb-4">
                    <h1 className="mb-2">Region</h1>
                    {locations.map((location: Location) => (<p><Link href={{ pathname: '/search', query: { ...queries, city: location.name } }} className='font-light text-reg capitalize cursor-pointer'>{location.name}</Link></p>))}
                </div>
                <div className="border-b pb-4 mt-3">
                    <h1 className="mb-2">Cuisine</h1>
                    {cuisines.map((cuisine: Cuisine) => (<p><Link href={{ pathname: '/search', query: { ...queries, cuisine: cuisine.name } }} className="font-light text-reg capitalize cursor-pointer">{cuisine.name}</Link></p>))}
                </div>
                <div className="mt-3 pb-4">
                    <h1 className="mb-2">Price</h1>
                    <div className="flex">
                        <Link href={{ pathname: '/search', query: { ...queries, price: prices.CHEAP } }} className="border w-full text-reg font-light rounded-l p-2">
                            $
                        </Link >
                        <Link href={{ pathname: '/search', query: { ...queries, price: prices.REGULAR } }}
                            className="border-r border-t border-b w-full text-reg font-light p-2"
                        >
                            $$
                        </Link>
                        <Link href={{ pathname: '/search', query: { ...queries, price: prices.EXPENSIVE } }}
                            className="border-r border-t border-b w-full text-reg font-light p-2 rounded-r"
                        >
                            $$$
                        </Link>
                    </div>
                </div>
            </div>
        </>
    );
}