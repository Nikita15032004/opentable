'use client'
import React, { useState } from 'react';
import { useRouter } from 'next/navigation';
//////////////////////////////////
export default function SearchBar() {
    const router = useRouter();
    const [location, setLocation] = useState<string>('');

    const searchHadler = (e: React.ChangeEvent<HTMLInputElement>) => {
        setLocation(e.target.value);
    }

    const onSearchClick = () => {
        if (location === '') return;
        router.push(`/search?city=${location}`)
    }
    return (
        <>
            <div className="text-left text-lg py-3 m-auto flex justify-center">
                <input onChange={searchHadler}
                    className="rounded  mr-3 p-2 w-[450px]"
                    type="text"
                    placeholder="State, city or town"
                    value={location}
                />
                <button onClick={onSearchClick} className="rounded bg-red-600 px-9 py-2 text-white">
                    Let's go
                </button>
            </div>
        </>
    );
}