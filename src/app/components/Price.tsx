import { PRICE } from '@prisma/client';
import React from 'react';
//////////////////////////////////
export default function Price({ price }: { price: PRICE }) {
    let priceSign = <span>$$$$</span>;
    if (price === "CHEAP") {
        priceSign = <span >$$<span className='text-gray-500'>$$</span></span>
    } else if (price === "REGULAR") {
        priceSign = <span >$$$<span className='text-gray-500'>$</span></span>
    }
    return (
        <>
            {priceSign}
        </>
    );
}