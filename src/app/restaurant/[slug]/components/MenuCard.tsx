import { Item } from '@prisma/client';
import React from 'react';
//////////////////////////////////
export default function MenuCard({ restaurantItem }: { restaurantItem: Item}) {
    return (
        <>
            <div className=" border rounded p-3 w-[49%] mb-3">
                <h3 className="font-bold text-lg">{restaurantItem.name}</h3>
                <p className="font-light mt-1 text-sm">
                    {restaurantItem.description}
                </p>
                <p className="mt-7">{restaurantItem.price}</p>
            </div>
        </>
    );
}