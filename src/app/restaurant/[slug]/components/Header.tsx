import React from 'react';
//////////////////////////////////
export default function Header({ name }: { name: string }) {
    function reorganiseName(str: string): string {
        let result = str.replace(/-/g, ' ');
        let words = result.split(' ');
        let capitalizedWords = words.map(word => word[0].toUpperCase() + word.slice(1));
        let lastWord = capitalizedWords.pop();
        capitalizedWords.push('(' + lastWord + ')');
        return capitalizedWords.join(' ');
    }
    return (
        <>
            <div className="h-96 overflow-hidden">
                <div
                    className="bg-center bg-gradient-to-r from-[#0f1f47] to-[#5f6984] h-full flex justify-center items-center"
                >
                    <h1 className="text-7xl text-white captitalize text-shadow text-center">
                        {reorganiseName(name)}
                    </h1>
                </div>
            </div>
        </>
    );
}